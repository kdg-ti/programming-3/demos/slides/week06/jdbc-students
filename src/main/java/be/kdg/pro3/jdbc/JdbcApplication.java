package be.kdg.pro3.jdbc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

//@SpringBootApplication
public class JdbcApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(JdbcApplication.class, args);
	}

	public void runBasic(String... args) throws Exception {
		Connection connection = DriverManager.getConnection("jdbc:h2:file:./db/studentsdb");
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS");
		while (resultSet.next()) {
			System.out.println(resultSet.getString("ID") + " " + resultSet.getString("NAME"));
		}
		resultSet.close();
		statement.close();
		connection.close();
	}

	@Override
	public void run(String... args) throws Exception {
		try (Connection connection = DriverManager.getConnection("jdbc:h2:file:./db/studentsdb")) {
			try (Statement statement = connection.createStatement()) {
				int result = statement.executeUpdate("INSERT INTO STUDENTS(NAME, LENGTH, BIRTHDAY) " +
					"VALUES('AN',1.65,'1967-3-12')");
				System.out.println(result  + " row(s) updated");
			}
		}
		try (Connection connection = DriverManager.getConnection("jdbc:h2:file:./db/studentsdb");
		     Statement statement = connection.createStatement();
		) {
			try (ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS")) {
				while (resultSet.next()) {
					System.out.println(resultSet.getString("ID") + " " + resultSet.getString("NAME"));
					System.out.println(resultSet.getString(1) + " " + resultSet.getString(2));
				}
			}
		}
		try (Connection connection = DriverManager.getConnection("jdbc:h2:file:./db/studentsdb")) {
			try (Statement statement = connection.createStatement()) {
				int result = statement.executeUpdate("DELETE FROM STUDENTS WHERE NAME = 'AN'");
				System.out.println(result + " row(s) updated");
			}
		}

		}

	}

